FROM jboss/wildfly
# set timezone
USER root
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
USER jboss
# replace configuration with our's with remote ActiveMQ settings
COPY wildfly-eclinicinfo-config.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml
# deploy app
COPY target/eclinic-info-1.0.0.-SNAPSHOT.war /opt/jboss/wildfly/standalone/deployments/eclinic-info.war
# add user to enable management
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
# restart server
CMD ["/opt/jboss/wildfly/bin/standalone.sh",  "-c", "standalone.xml", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]