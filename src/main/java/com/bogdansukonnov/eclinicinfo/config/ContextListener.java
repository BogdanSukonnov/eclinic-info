package com.bogdansukonnov.eclinicinfo.config;

import com.bogdansukonnov.eclinicinfo.service.EventService;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

    @Inject
    private EventService eventService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        eventService.updateEvents();
    }
}
