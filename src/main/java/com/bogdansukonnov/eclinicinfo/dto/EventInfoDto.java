package com.bogdansukonnov.eclinicinfo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventInfoDto {
    private Long eventId;
    private String time;
    private String treatmentType;
    private String treatment;
    private String patient;
}
