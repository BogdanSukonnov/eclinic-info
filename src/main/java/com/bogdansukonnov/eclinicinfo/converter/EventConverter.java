package com.bogdansukonnov.eclinicinfo.converter;

import com.bogdansukonnov.eclinicinfo.dto.EventInfoDto;
import com.bogdansukonnov.eclinicinfo.entity.Event;

import javax.ejb.Singleton;

@Singleton
public class EventConverter {

    public Event toEvent(EventInfoDto dto) {
        Event event = new Event();
        event.setEventId(dto.getEventId());
        event.setTreatment(dto.getTreatment());
        event.setPatient(dto.getPatient());
        event.setTime(dto.getTime());
        event.setTreatmentType(dto.getTreatmentType());
        return event;
    }
}
