package com.bogdansukonnov.eclinicinfo;

import com.bogdansukonnov.eclinicinfo.config.PropertiesFromFile;
import com.bogdansukonnov.eclinicinfo.converter.EventConverter;
import com.bogdansukonnov.eclinicinfo.dto.EventInfoListDto;
import com.bogdansukonnov.eclinicinfo.entity.Event;
import com.bogdansukonnov.eclinicinfo.service.EventService;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class EventsBean {

    private List<Event> events = new ArrayList<>();
    private String updated = "never";

    @Inject
    @PropertiesFromFile
    private Properties appProperties;

    @Inject
    private EventConverter converter;

    @Inject
    @Push
    private PushContext push;

    @Inject
    private Logger log;

    public void onEventInfoListUpdate(@Observes EventInfoListDto eventsDTO) {
        log.info("fired event on EventInfoListDto");
        updated = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        events.clear();
        if (eventsDTO != null) {
            events = eventsDTO.getEvents().stream()
                    .map(dto -> converter.toEvent(dto))
                    .collect(Collectors.toList());
        }
        push.send("updateEvents");
    }

    public List<Event> getEvents() {
        return events;
    }

    public String getCompanyName() {
        return appProperties.getProperty("company.name");
    }

    public  String getUpdated() {
        return updated;
    }

}
