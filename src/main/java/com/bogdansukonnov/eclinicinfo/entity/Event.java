package com.bogdansukonnov.eclinicinfo.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Event {

    private Long eventId;
    private String time;
    private String treatmentType;
    private String treatment;
    private String patient;

}
