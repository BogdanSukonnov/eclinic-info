package com.bogdansukonnov.eclinicinfo.message;

import com.bogdansukonnov.eclinicinfo.service.EventService;
import org.jboss.ejb3.annotation.ResourceAdapter;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@ResourceAdapter("remote-artemis")
@MessageDriven(name = "EventChangeListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "useJNDI", propertyValue = "false"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "event-update"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class JMSListenerBean implements MessageListener {

    @Inject
    private Logger log;

    @Inject
    EventService eventService;

    public JMSListenerBean() {
    }

    @Override
    public void onMessage(Message message) {
        log.info("message received " + log.getClass());
        eventService.updateEvents();
    }

}
