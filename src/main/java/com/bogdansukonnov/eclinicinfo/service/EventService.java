package com.bogdansukonnov.eclinicinfo.service;

import com.bogdansukonnov.eclinicinfo.RestClient;
import com.bogdansukonnov.eclinicinfo.dto.EventInfoListDto;

import javax.ejb.Singleton;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import java.util.logging.Logger;

@Singleton
public class EventService {

    @Inject
    private BeanManager beanManager;

    @Inject
    private RestClient restClient;

    @Inject
    private Logger log;

    public void updateEvents() {
        EventInfoListDto eventsDTO = null;
        try {
            eventsDTO = restClient.getEvents();
        } catch (Exception e) {
            log.warning(e.toString() + " - " + e.getMessage() + " : " + e.getStackTrace().toString());
        }
        beanManager.fireEvent(eventsDTO);
    }
}