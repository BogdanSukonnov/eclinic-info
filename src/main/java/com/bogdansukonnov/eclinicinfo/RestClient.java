package com.bogdansukonnov.eclinicinfo;

import com.bogdansukonnov.eclinicinfo.config.PropertiesFromFile;
import com.bogdansukonnov.eclinicinfo.dto.EventInfoListDto;
import lombok.Getter;
import lombok.Setter;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Properties;
import java.util.logging.Logger;

@Singleton
@Getter
@Setter
public class RestClient {

    @Inject
    @PropertiesFromFile
    private Properties appProperties;

    @Inject
    private Logger log;

    public EventInfoListDto getEvents() {
        String mainAppUrl = appProperties.getProperty("mainApp.url");
        log.info("getting events from " + mainAppUrl);
        // do request
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(mainAppUrl);
        Invocation.Builder builder = target.request();
        builder.accept(MediaType.APPLICATION_JSON);
        Response response = builder.get();
        // get DTO
        EventInfoListDto eventInfoListDto = response.readEntity(EventInfoListDto.class);
        // close response
        response.close();
        log.info("events successfully received");
        return eventInfoListDto;
    }
}
